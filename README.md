# Ruby with tests

This repository is only a playground to study tests with Ruby

## Unit Tests with test/unit/assertions

Simple example with tests from hello execution.

## How to Run

```bash
ruby testUnit.rb
```

Fail Return Example:

![Fail](resources/images/TestUnitError.png)

## BDD with Rspec

### Pre-requisites

* rspec - `gem install rspec`

### How to Run

```bash
cd spec
rspec hello_spec.rb
```

Success Return Example:

![Success](resources/images/bdd-success.png)

Failed Return Example:

![Fail](resources/images/bdd-fail.png)

## MiniTest

## Pre-requisites

* minitest - `gem install minitest`

### Unit Test

```bash
ruby hello_test.rb
```

Success Return Example:

![Success](resources/images/minitest-unity-success.png)

Fail Return Example:

![Fail](resources/images/minitest-unity-fail.png)

### BDD

```bash
ruby hello_rspec.rb
```

Fail Return Example:

![Fail](resources/images/minitest-bdd-fail.png)